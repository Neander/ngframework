Framework:

- EntryPoint/CentralPoint
- Scene System
  - Scene callbacks
  - Scene loading with transitions
- UI System
  - Layers system
  - UI elements with animation
  - UI Library -> refs to prefabs
- Input System
- Audio System
  - Audio Libraries
  - Play/Pause/Stop functionality
- Pathfinding System
- Search and Replace Tools

Optional:

- Networking System
- Mobile elements
  - Ratings
  - Push notificaions
