﻿using NGFramework.Core;
using NGFramework.Core.SceneManagement;
using NGFramework.Core.UI;
using NGFramework.SceneManagement;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : UIElement
{
    public override event Action<IUIElement> ElementPoped;

    [SerializeField]
    private Button sceneLoadButton;

    public override IUIElement CreateInstance(Transform root)
    {
        MainMenuUI instance = Instantiate(this, root);
        instance.Initialize();
        return instance;
    }

    public override void RemoveInstance()
    {
        StartCoroutine(AnimateOut());
    }

    private void Initialize()
    {
        sceneLoadButton.onClick.AddListener(() =>
        {
            SceneController sceneController = ServiceLocator.Resolve<SceneController>();
            sceneController.LoadSceneAsync(Scenes.MainScene);
        });
    }

    private IEnumerator AnimateOut()
    {
        float t = 5;
        while (t >= 0)
        {
            t -= Time.deltaTime;
            transform.Rotate(Vector3.forward, (360 / 5) * Time.deltaTime);
            yield return null;
        }

        Destroy(gameObject);
        ElementPoped?.Invoke(this);
    }
}