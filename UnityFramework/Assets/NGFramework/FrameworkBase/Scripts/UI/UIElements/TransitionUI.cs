﻿using NGFramework.Core.UI;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TransitionUI : UIElement
{
    public enum FadeMode
    {
        UNDEFINED,
        IN,
        OUT
    }

    public override event Action<IUIElement> ElementPoped;

    [SerializeField]
    private Image transitionImage;

    [SerializeField]
    private float fadeTime = 2;

    private bool fading;

    public override IUIElement CreateInstance(Transform root)
    {
        return Instantiate(this, root);
    }

    public override void RemoveInstance()
    {
        Destroy(gameObject);
        ElementPoped?.Invoke(this);
    }

    public void StartFade(FadeMode fadeMode, Action callback = null)
    {
        if (fading)
        {
            StopAllCoroutines();
        }

        StartCoroutine(Fade(fadeMode, callback));
    }

    private IEnumerator Fade(FadeMode fadeMode, Action doneAction = null)
    {
        fading = true;

        float t = fadeTime;

        Color fromColor = new Color(transitionImage.color.r, transitionImage.color.g, transitionImage.color.b, fadeMode == FadeMode.IN ? 0 : 1);
        Color toColor = new Color(transitionImage.color.r, transitionImage.color.g, transitionImage.color.b, fadeMode == FadeMode.IN ? 1 : 0);

        while (t >= 0)
        {
            t -= Time.deltaTime;
            transitionImage.color = Color.Lerp(fromColor, toColor, t / fadeTime);
            yield return null;
        }

        fading = false;
        doneAction?.Invoke();
    }
}