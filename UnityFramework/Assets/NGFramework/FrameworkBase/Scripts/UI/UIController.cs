﻿using NGFramework.Core;
using NGFramework.Core.SceneManagement;
using NGFramework.Core.UI;
using NGFramework.SceneManagement;
using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace NGFramework.UI
{
    public enum UILayers
    {
        UNDEFINED,
        MainLayer,
        TransitionLayer
    }

    public class UIController : MonoBehaviour, IController
    {
        public Transform UIRoot { get; private set; }
        public EventSystem EventSystem { get; private set; }

        public bool ReadyForTransition { get; private set; }

        [SerializeField]
        private GameObject uiRootPrefab;

        [SerializeField]
        private GameObject eventSystemPrefab;

        [SerializeField]
        private UILibrary uiLibrary;

        private IUIService uiService;
        private SceneController sceneController;

        public void Initialize(Action<string> onIntializedCallback)
        {
            DontDestroyOnLoad(gameObject);

            ServiceLocator.Register<UIController>(this);
            sceneController = ServiceLocator.Resolve<SceneController>();
            sceneController.SceneLoadingStarted += SceneLoadingStarted;

            UIRoot = Instantiate(uiRootPrefab, transform).transform;
            EventSystem = Instantiate(eventSystemPrefab, transform).GetComponent<EventSystem>();

            uiService = new UIService();
            uiService.LayerCreated += OnLayerCreated;

            CreateLayer(UILayers.MainLayer);
            CreateLayer(UILayers.TransitionLayer);

            onIntializedCallback?.Invoke("UIController");
        }

        public UIElement PushElement(string indicator, UILayer layer, bool persistent = false)
        {
            IUIElement element = uiLibrary.GetElement(indicator);
            if (element == null)
            {
                element = uiLibrary.GetElementByAlias(indicator);
            }

            if (element != null)
            {
                return layer.PushElement(element, persistent) as UIElement;
            }

            return null;
        }

        public void CreateLayer(UILayers layer, bool overlay = false)
        {
            uiService.CreateLayer(layer.ToString(), overlay);
        }

        public UILayer GetLayer(UILayers layer)
        {
            return uiService.GetLayer(layer.ToString()) as UILayer;
        }

        public UILayer GetLayer(string name)
        {
            return uiService.GetLayer(name) as UILayer;
        }

        public void RemoveLayer(string name)
        {
            IUILayer layer = uiService.GetLayer(name);
            uiService.RemoveLayer(layer, () =>
            {
                LayerRemoved(layer);
            });
        }

        public void RemoveLayer(IUILayer uiLayer)
        {
            RemoveLayer(uiLayer.LayerName);
        }

        private void OnLayerCreated(IUILayer layer)
        {
            GameObject layerGameObject = new GameObject(layer.LayerName);
            RectTransform rectTransform = layerGameObject.AddComponent<RectTransform>();
            rectTransform.SetParent(UIRoot);
            rectTransform.localScale = Vector3.one;

            rectTransform.anchorMin = new Vector2(0, 0);
            rectTransform.anchorMax = new Vector2(1, 1);
            rectTransform.pivot = new Vector2(0.5f, 0.5f);

            rectTransform.anchoredPosition = Vector2.zero;
            rectTransform.sizeDelta = Vector2.zero;

            layer.SetLayerGameObject(layerGameObject);
        }

        private void LayerRemoved(IUILayer layer)
        {
            Destroy(layer.LayerGameObject);
        }

        private void SceneLoadingStarted(Scenes scene)
        {
            ReadyForTransition = false;

            IUILayer[] layers = uiService.GetLayers();
            int layersToPop = layers.Length;
            foreach (var layer in layers)
            {
                layer.PopLayer(() =>
                {
                    Debug.LogFormat("Poped layer: {0}", layer.LayerName);
                    layersToPop--;
                    if (layersToPop <= 0)
                    {
                        ReadyForTransition = true;
                        sceneController.LoadSceneWhenDone();
                    }
                });
            }
        }

        private void OnDestroy()
        {
            uiService.LayerCreated -= OnLayerCreated;
            sceneController.SceneLoadingStarted -= SceneLoadingStarted;
        }
    }
}