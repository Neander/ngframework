﻿using NGFramework.Core;
using NGFramework.Core.Sound;
using NGFramework.SceneManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NGFramework.Sound
{
    public class SoundController : MonoBehaviour, ISoundController, IController
    {
        public event Action<string, long, float, bool, bool, int> OnPlayAudio;

        public event Action<string, long, AudioSource> OnPlayExternalSource;

        public event Action<long, bool> OnStopAudio;

        public event Action<long, bool> OnPauseAudio;

        public event Action<long, float> OnChangeAudioVolume;

        public event Action<long, bool> OnMuteAudio;

        public bool MuteState { get; private set; }

        [SerializeField]
        private AudioLibrary audioLibrary;

        private SceneController sceneController;
        private ISoundService soundService;

        public void Initialize(Action<string> onIntializedCallback)
        {
            ServiceLocator.Register<ISoundController>(this);

            CreateAndInitializeSoundRepository();
            soundService = new SoundService();

            onIntializedCallback?.Invoke("SoundController");
        }

        public SoundClipMeta PlayAudio(string soundClipId, AudioSource externalSource)
        {
            SoundClip soundClip = audioLibrary.clips.FirstOrDefault(c => c.id == soundClipId);
            if (soundClip != null)
            {
                if (soundClip.clip)
                {
                    long id = soundService.AddAudio();
                    if (id >= 0)
                    {
                        OnPlayExternalSource?.Invoke(soundClipId, id, externalSource);
                        return new SoundClipMeta(id, soundClip.clip.length);
                    }
                }
                else
                {
                    Debug.LogFormat("No clip attached to the clip with id {0}", soundClipId);
                    return null;
                }
            }
            Debug.LogFormat("No clip with id {0} found in the combined library.", soundClipId);
            return null;
        }

        public SoundClipMeta PlayAudio(string soundClipId, float volume = 1, bool loop = false, int repeatAmount = 0)
        {
            SoundClip soundClip = audioLibrary.clips.FirstOrDefault(c => c.id == soundClipId);
            if (soundClip != null)
            {
                if (soundClip.clip)
                {
                    long id = soundService.AddAudio();
                    if (id >= 0)
                    {
                        OnPlayAudio?.Invoke(soundClipId, id, volume, MuteState, loop, repeatAmount);
                        return new SoundClipMeta(id, soundClip.clip.length);
                    }
                }
                else
                {
                    Debug.LogFormat("No clip attached to the clip with id {0}", soundClipId);
                    return null;
                }
            }
            Debug.LogFormat("No clip with id {0} found in the combined library.", soundClipId);
            return null;
        }

        public void StopAll(bool fade = false)
        {
            IEnumerable<ISound> allAudio = soundService.GetAllPlayingAudio();

            foreach (ISound audio in allAudio)
            {
                OnStopAudio?.Invoke(audio.Id, fade);
            }
        }

        public void StopAudio(long id, bool fade = false)
        {
            ISound audio = soundService.GetPlayingAudio(id);
            if (audio != null)
            {
                OnStopAudio?.Invoke(audio.Id, fade);
            }
        }

        public void SetMutedState(bool muted)
        {
            MuteState = muted;

            IEnumerable<ISound> allAudio = soundService.GetAllPlayingAudio();
            foreach (ISound audio in allAudio)
            {
                OnMuteAudio?.Invoke(audio.Id, muted);
            }
        }

        public void SetAudioVolume(float volume)
        {
            IEnumerable<ISound> allAudio = soundService.GetAllPlayingAudio();
            foreach (ISound audio in allAudio)
            {
                OnChangeAudioVolume?.Invoke(audio.Id, volume);
            }
        }

        public void PauseAll(bool paused)
        {
            IEnumerable<ISound> allAudio = soundService.GetAllPlayingAudio();

            foreach (ISound audio in allAudio)
            {
                OnPauseAudio?.Invoke(audio.Id, paused);
            }
        }

        public void PauseAudio(long id, bool paused)
        {
            ISound audio = soundService.GetPlayingAudio(id);
            if (audio != null)
            {
                OnPauseAudio?.Invoke(audio.Id, paused);
            }
        }

        private void OnScenePause(bool pauseState)
        {
            PauseAll(pauseState);
        }

        private void CreateAndInitializeSoundRepository()
        {
            GameObject soundRepoObject = new GameObject("SoundRepository");
            soundRepoObject.transform.SetParent(transform);

            SoundRepository soundRepository = soundRepoObject.AddComponent<SoundRepository>();
            soundRepository.Intitialize(audioLibrary);
        }
    }
}