﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class ObjectReplacerEditor : EditorWindow
{
    [MenuItem("NGFramework/Object Replacer")]
    public static void Init()
    {
        GetWindow(typeof(ObjectReplacerEditor), false, "Object Replacer", true);
    }

    private string searchText = "";
    private string previousSearch = "";
    private Vector2 scrollPos = Vector2.zero;

    private bool replacing;
    private string selectedPrefabPath = "";
    private GameObject selectedObject = null;

    private void Update()
    {
        if (selectedObject != Selection.activeObject)
        {
            selectedObject = Selection.activeGameObject;
            Repaint();
        }
    }

    private void OnGUI()
    {
        if (replacing)
        {
            GUILayout.Label("Working on things . . .");
            return;
        }

        searchText = EditorGUILayout.TextField("Scene Item Name: ", searchText);

        GameObject[] sceneObjects = FindObjectsOfType<GameObject>();

        if (sceneObjects != null && sceneObjects.Length > 0)
        {
            IEnumerable<GameObject> results = sceneObjects.Where(x =>
            {
                if (x.name.ToLower().Split('(')[0].Replace(" ", "") == searchText.ToLower())
                {
                    return true;
                }
                else
                {
                    return false;
                }
            });

            if (selectedObject != null || (results != null && results.Count() > 0))
            {
                EditorGUILayout.LabelField("Replace with: ");

                string[] prefabs = GetAllPrefabs();
                if (previousSearch != searchText)
                {
                    scrollPos = Vector2.zero;
                }

                scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
                foreach (var prefab in prefabs)
                {
                    EditorGUILayout.BeginHorizontal();

                    GameObject gO = AssetDatabase.LoadAssetAtPath<GameObject>(prefab);
                    if (gO != null)
                    {
                        Texture2D preview = AssetPreview.GetAssetPreview(gO);
                        if (preview != null)
                        {
                            GUILayout.Label(preview);

                            if (GUILayout.Button(Path.GetFileNameWithoutExtension(prefab)))
                            {
                                selectedPrefabPath = prefab;
                            }
                        }
                    }

                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndScrollView();

                if (!string.IsNullOrEmpty(selectedPrefabPath))
                {
                    EditorGUILayout.LabelField("Selected Prefab: " + Path.GetFileNameWithoutExtension(selectedPrefabPath));
                }
            }

            EditorGUILayout.BeginHorizontal();

            EditorGUI.BeginDisabledGroup(results == null || results.Count() <= 0 || string.IsNullOrEmpty(selectedPrefabPath));
            if (GUILayout.Button("Replace All"))
            {
                ReplaceAll(results.ToArray());
            }
            EditorGUI.EndDisabledGroup();

            EditorGUI.BeginDisabledGroup(selectedObject == null || string.IsNullOrEmpty(selectedPrefabPath));
            if (GUILayout.Button("Replace Selected"))
            {
                ReplaceSingle(selectedObject);
            }
            EditorGUI.EndDisabledGroup();

            EditorGUILayout.EndHorizontal();
        }

        previousSearch = searchText;
    }

    private void ReplaceAll(GameObject[] sceneObjects)
    {
        replacing = true;

        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(selectedPrefabPath);

        foreach (var sO in sceneObjects)
        {
            GameObject newObj = Instantiate(prefab, sO.transform.parent);
            newObj.transform.localPosition = sO.transform.localPosition;
            newObj.transform.localRotation = sO.transform.localRotation;
            newObj.transform.localScale = sO.transform.localScale;
            newObj.transform.SetSiblingIndex(sO.transform.GetSiblingIndex());

            Undo.RegisterCreatedObjectUndo(newObj, "Undo object replacer new object");
            Undo.DestroyObjectImmediate(sO);
        }

        replacing = false;
    }

    private void ReplaceSingle(GameObject sO)
    {
        replacing = true;

        if (sO != null)
        {
            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(selectedPrefabPath);

            GameObject newObj = Instantiate(prefab, sO.transform.parent);
            newObj.transform.localPosition = sO.transform.localPosition;
            newObj.transform.localRotation = sO.transform.localRotation;
            newObj.transform.localScale = sO.transform.localScale;
            newObj.transform.SetSiblingIndex(sO.transform.GetSiblingIndex());

            Undo.RegisterCreatedObjectUndo(newObj, "Undo object replacer new object");
            Undo.DestroyObjectImmediate(sO);
        }

        replacing = false;
    }

    private string[] GetAllPrefabs()
    {
        string[] paths = AssetDatabase.GetAllAssetPaths();
        List<string> results = new List<string>();

        foreach (var path in paths)
        {
            if (Path.GetExtension(path) == ".prefab")
            {
                results.Add(path);
            }
        }

        return results.ToArray();
    }
}