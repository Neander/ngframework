﻿using NGFramework.Core;
using NGFramework.Core.SceneManagement;
using System;
using UnityEngine;

namespace NGFramework.SceneManagement
{
    public class SceneController : MonoBehaviour, IController
    {
        public event Action<Scenes> SceneLoadingStarted;

        public event Action<Scenes> SceneLoadingDone;

        public Scenes ActiveScene { get { return sceneService.ActiveScene; } }

        [SerializeField]
        private Scenes startScene;

        private SceneService sceneService;
        private ProjectContext projectContext;

        public void Initialize(Action<string> onIntializedCallback)
        {
            ServiceLocator.Register<SceneController>(this);

            sceneService = new SceneService();
            projectContext = ServiceLocator.Resolve<ProjectContext>();
            projectContext.InitializationDoneEventHandler += InitializationDone;

            onIntializedCallback?.Invoke("SceneController");
        }

        private void InitializationDone()
        {
            LoadScene(startScene);
        }

        /// <summary>
        /// Immediatly loads the scene without waiting for UI animations to finish.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="callback"></param>
        public void LoadScene(Scenes scene, Action<Scenes> callback = null)
        {
            SceneLoadingStarted?.Invoke(scene);
            Debug.LogFormat("Started loading <color=yellow>{0}</color>", scene);

            sceneService.LoadScene(scene, () =>
            {
                Debug.LogFormat("Loaded <color=green>{0}</color>", scene);
                callback?.Invoke(scene);
                SceneLoadingDone?.Invoke(scene);
            });
        }

        /// <summary>
        /// Loads the scene after the UI animations have finished.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="callback"></param>
        public void LoadSceneAsync(Scenes scene, Action<Scenes> callback = null)
        {
            SceneLoadingStarted?.Invoke(scene);
            Debug.LogFormat("Started loading <color=yellow>{0}</color>", scene);

            sceneService.LoadSceneAsync(scene, () =>
            {
                Debug.LogFormat("Loaded <color=green>{0}</color>", scene);
                callback?.Invoke(scene);
                SceneLoadingDone?.Invoke(scene);
            });
        }

        public void LoadSceneWhenDone()
        {
            sceneService.LoadSceneWhenDone();
        }
    }
}