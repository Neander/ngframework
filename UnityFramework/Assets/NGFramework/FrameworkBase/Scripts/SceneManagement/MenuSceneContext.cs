﻿using NGFramework.Core;
using NGFramework.Core.UI;
using NGFramework.UI;
using UnityEngine;

public class MenuSceneContext : MonoBehaviour
{
    private void Awake()
    {
        UIController uiController = ServiceLocator.Resolve<UIController>();

        UILayer transitionLayer = uiController.GetLayer(UILayers.TransitionLayer);
        UILayer mainLayer = uiController.GetLayer(UILayers.MainLayer);

        MainMenuUI mainMenuInstance = uiController.PushElement("MainMenuUI", mainLayer) as MainMenuUI;

        TransitionUI transitionUI = uiController.PushElement("TransitionUI", transitionLayer, true) as TransitionUI;
        transitionUI.StartFade(TransitionUI.FadeMode.IN, () =>
        {
            transitionUI.RemoveInstance();
        });
    }
}