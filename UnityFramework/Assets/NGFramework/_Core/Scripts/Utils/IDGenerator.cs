﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace NGFramework.Core.Utils
{
    public static class IDGenerator
    {
        private static int generationCount;

        private static List<string> currentStringIDs = new List<string>();
        private static List<long> currentNumberIDs = new List<long>();

        public static string GenerateUniqueID(int lenght)
        {
            StringBuilder builder = new StringBuilder();

            Enumerable
                .Range(65, 26)
                .Select(e => ((char)e).ToString())
                .Concat(Enumerable.Range(97, 26).Select(e => ((char)e).ToString()))
                .Concat(Enumerable.Range(0, 10).Select(e => e.ToString()))
                .OrderBy(e => Guid.NewGuid())
                .Take(lenght)
                .ToList().ForEach(e => builder.Append(e));

            string idString = builder.ToString();
            if (currentStringIDs.Contains(idString))
            {
                generationCount++;
                if (generationCount >= 25)
                {
                    Debug.LogErrorFormat("Could not generate a unique id after {0} attempts. Aborting!", generationCount);
                    return "";
                }
                return GenerateUniqueID(lenght);
            }
            return idString;
        }

        public static long GenerateUniqueNumberID()
        {
            long id = currentNumberIDs.Count + 1;
            if (currentNumberIDs.Contains(id))
            {
                return GenerateUniqueNumberID();
            }
            currentNumberIDs.Add(id);
            return id;
        }
    }
}