﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NGFramework.Core
{
    /// <summary>
    /// The entry point of the application.
    /// </summary>
    public class ProjectContext : MonoBehaviour
    {
        public event Action InitializationDoneEventHandler;

        public bool Paused { get; private set; }

        private int initializedCount = 0;

        private void Awake()
        {
            ServiceLocator.Register<ProjectContext>(this);

            IController[] controllers = FindObjectsByInterface<IController>();

            DontDestroyOnLoad(this);

            foreach (var controller in controllers)
            {
                (controller as IController).Initialize((name) =>
                {
                    Debug.LogFormat("<color=blue>Initialized</color> {0}", name);
                    initializedCount++;

                    if (initializedCount == controllers.Length)
                    {
                        Debug.Log("<color=green>Initalization Done!</color>");
                        InitializationDoneEventHandler?.Invoke();
                    }
                });
            }
        }

        public static T[] FindObjectsByInterface<T>()
        {
            List<T> interfaces = new List<T>();
            GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            foreach (var rootGameObject in rootGameObjects)
            {
                T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>();
                foreach (var childInterface in childrenInterfaces)
                {
                    interfaces.Add(childInterface);
                }
            }
            return interfaces.ToArray();
        }
    }
}