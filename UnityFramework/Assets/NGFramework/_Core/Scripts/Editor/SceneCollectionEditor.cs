﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace NGFramework.Core.Editor
{
    [InitializeOnLoad]
    internal class SceneCollectionEditor
    {
        private const string scenesFileName = "Scenes.cs";

        private const string fileFolderLocation = "Scenes";

        private static string[] beginLines = new string[] {
        "namespace NGFramework.Core.SceneManagement",
        "{",
        "    /// <summary>",
        "    /// Do not edit this file because it is auto generated!",
        "    /// </summary>",
        "    public enum Scenes",
        "    {"
    };

        private static string fileLocation;

        [InitializeOnLoadMethod]
        private static void Intit()
        {
            fileLocation = Path.Combine(Application.dataPath, fileFolderLocation);
            EditorBuildSettings.sceneListChanged += SceneListChanged;
        }

        [MenuItem("NGFramework/Scene Management/Force Scene List Update")]
        private static void ForceSceneListUpdate()
        {
            SceneListChanged();
        }

        private static void SceneListChanged()
        {
            if (!Directory.Exists(fileLocation))
            {
                Directory.CreateDirectory(fileLocation);
            }

            using (var streamWriter = new StreamWriter(Path.Combine(fileLocation, scenesFileName)))
            {
                for (int i = 0; i < beginLines.Length; i++)
                {
                    streamWriter.WriteLine(beginLines[i]);
                }

                for (int j = 0; j < EditorBuildSettings.scenes.Length; j++)
                {
                    streamWriter.WriteLine("        " + Path.GetFileNameWithoutExtension(EditorBuildSettings.scenes[j].path) + (j == EditorBuildSettings.scenes.Length - 1 ? "" : ","));
                }

                streamWriter.WriteLine("    }");
                streamWriter.WriteLine("}");
                streamWriter.Close();
            }

            AssetDatabase.Refresh();
            Debug.Log("Scenes enum regenerated");
        }
    }
}