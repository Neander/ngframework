﻿using System;

namespace NGFramework.Core.UI
{
    public interface IUIService
    {
        event Action<IUILayer> LayerCreated;

        event Action<IUILayer> LayerRemoved;

        IUILayer GetLayer(string name);

        IUILayer[] GetLayers();

        IUILayer CreateLayer(string name, bool overlay);

        void RemoveLayer(IUILayer layer, Action callback);
    }
}