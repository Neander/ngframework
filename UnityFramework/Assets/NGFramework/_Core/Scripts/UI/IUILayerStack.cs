﻿using System;
using System.Collections.Generic;

namespace NGFramework.Core.UI
{
    public interface IUILayerStack
    {
        List<IUILayer> Layers { get; }

        void PushLayer(IUILayer layer);

        void PushOverlay(IUILayer layer);

        void PopLayer(IUILayer layer, Action callback);

        void PopOverlay(IUILayer layer, Action callback);
    }
}