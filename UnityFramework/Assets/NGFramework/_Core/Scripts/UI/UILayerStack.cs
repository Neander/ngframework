﻿using System;
using System.Collections.Generic;

namespace NGFramework.Core.UI
{
    public class UILayerStack : IUILayerStack
    {
        public List<IUILayer> Layers { get; private set; }

        private int layerInsertIndex = 0;

        public UILayerStack()
        {
            Layers = new List<IUILayer>();
        }

        public void PushLayer(IUILayer layer)
        {
            Layers.Insert(layerInsertIndex, layer);
            layerInsertIndex++;
        }

        public void PopLayer(IUILayer layer, Action callback)
        {
            if (Layers.Contains(layer))
            {
                Layers.Remove(layer);
                layer.PopLayer(callback);
            }
            layerInsertIndex--;
        }

        public void PushOverlay(IUILayer layer)
        {
            Layers.Add(layer);
        }

        public void PopOverlay(IUILayer layer, Action callback)
        {
            if (Layers.Contains(layer))
            {
                Layers.Remove(layer);
                layer.PopLayer(callback);
            }
        }
    }
}