﻿using System;
using System.Linq;
using UnityEngine;

namespace NGFramework.Core.UI
{
    [CreateAssetMenu(fileName = "UILibrary", menuName = "NGFramework/UILibrary")]
    public class UILibrary : ScriptableObject
    {
        public UILibraryElement[] uiElements = new UILibraryElement[0];

        public IUIElement GetElement(string id)
        {
            UILibraryElement libraryElement = uiElements.FirstOrDefault(x => x.ID == id);
            if (libraryElement != null && libraryElement.UIPrefab != null)
            {
                return libraryElement.UIPrefab as IUIElement;
            }
            return null;
        }

        public IUIElement GetElementByAlias(string alias)
        {
            UILibraryElement libraryElement = uiElements.FirstOrDefault(x => x.Alias == alias);
            if (libraryElement != null && libraryElement.UIPrefab != null)
            {
                return libraryElement.UIPrefab as IUIElement;
            }
            return null;
        }
    }

    [Serializable]
    public class UILibraryElement
    {
        public string ID { get; set; }
        public string Alias { get; set; }
        public UIElement UIPrefab { get; set; }

        public UILibraryElement()
        {
        }

        public UILibraryElement(string id)
        {
            ID = id;
        }
    }
}