﻿using NGFramework.Core.UI;
using NGFramework.Core.Utils;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace NGFramework.Core.Editor.UI
{
    [CustomEditor(typeof(UILibrary), true)]
    [CanEditMultipleObjects]
    public class UILibraryEditor : UnityEditor.Editor
    {
        private string searchText;
        private string loweredText;
        private bool isDirty;

        public override void OnInspectorGUI()
        {
            UILibrary targetLibrary = target as UILibrary;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Search: ", new GUILayoutOption[] { GUILayout.MaxWidth(50) });
            searchText = GUILayout.TextField(searchText);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);
            if (GUILayout.Button("Add UIElement Slot"))
            {
                List<UILibraryElement> elements = targetLibrary.uiElements.ToList();
                string id = IDGenerator.GenerateUniqueID(8);
                elements.Add(new UILibraryElement(id));
                targetLibrary.uiElements = elements.ToArray();

                isDirty = true;
            }
            GUILayout.Space(20);

            List<UILibraryElement> currentUIElements = targetLibrary.uiElements.ToList();
            UILibraryElement[] uiElements = targetLibrary.uiElements;
            if (!string.IsNullOrEmpty(searchText))
            {
                loweredText = searchText.ToLower();
                uiElements = targetLibrary.uiElements.Where(x =>
                {
                    if (x.ID.ToLower().Contains(loweredText) || x.Alias.ToLower().Contains(loweredText))
                    {
                        return true;
                    }
                    else if (x.UIPrefab != null && x.UIPrefab.name.ToLower().Contains(loweredText))
                    {
                        return true;
                    }
                    return false;
                }).ToArray();
            }

            foreach (UILibraryElement element in uiElements)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.SelectableLabel("ID: " + element.ID, new GUILayoutOption[] { GUILayout.Height(16) });
                EditorGUILayout.LabelField("Alias: ", new GUILayoutOption[] { GUILayout.MaxWidth(50) });
                element.Alias = EditorGUILayout.TextField(element.Alias);

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();

                element.UIPrefab = (UIElement)EditorGUILayout.ObjectField(element.UIPrefab, typeof(UIElement), false);

                if (GUILayout.Button("X", new GUILayoutOption[] { GUILayout.MaxWidth(30) }))
                {
                    currentUIElements.Remove(element);
                    isDirty = true;
                }

                EditorGUILayout.EndHorizontal();

                GUILayout.Space(10);
            }
            targetLibrary.uiElements = currentUIElements.ToArray();

            EditorUtility.SetDirty(targetLibrary);

            if (isDirty)
            {
                isDirty = false;
                AssetDatabase.SaveAssets();
            }
        }
    }
}