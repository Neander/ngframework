﻿using System;
using System.Linq;

namespace NGFramework.Core.UI
{
    public class UIService : IUIService
    {
        public event Action<IUILayer> LayerCreated;

        public event Action<IUILayer> LayerRemoved;

        private IUILayerStack layerStack;

        public UIService()
        {
            layerStack = new UILayerStack();
        }

        public IUILayer CreateLayer(string name, bool overlay)
        {
            IUILayer layer = new UILayer(name);
            if (overlay)
            {
                layerStack.PushOverlay(layer);
            }
            else
            {
                layerStack.PushLayer(layer);
            }

            LayerCreated?.Invoke(layer);

            return layer;
        }

        public IUILayer GetLayer(string name)
        {
            IUILayer layer = layerStack.Layers.First(x => x.LayerName == name);
            return layer;
        }

        public IUILayer[] GetLayers()
        {
            return layerStack.Layers.ToArray();
        }

        public void RemoveLayer(string name, Action callback)
        {
            IUILayer layer = GetLayer(name);
            RemoveLayer(layer, callback);
        }

        public void RemoveLayer(IUILayer layer, Action callback)
        {
            layerStack.PopLayer(layer, () =>
            {
                LayerRemoved?.Invoke(layer);
                callback?.Invoke();
            });
        }
    }
}