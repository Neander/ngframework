﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NGFramework.Core.UI
{
    public class UILayer : IUILayer
    {
        public string LayerName { get; private set; }

        public GameObject LayerGameObject { get; private set; }

        public List<IUIElement> UIElements { get; private set; }
        public List<IUIElement> PersistentUIElements { get; private set; }

        private int popCount = 0;

        public UILayer(string name)
        {
            LayerName = name;
            UIElements = new List<IUIElement>();
            PersistentUIElements = new List<IUIElement>();
        }

        /// <summary>
        /// If the persistent bool is set too true the element should be manually poped.
        /// </summary>
        /// <param name="uiElement"></param>
        /// <param name="persistent"></param>
        /// <returns></returns>
        public IUIElement PushElement(IUIElement uiElement, bool persistent = false)
        {
            IUIElement element = uiElement.CreateInstance(LayerGameObject.transform);
            element.ElementPoped += ElementPoped;

            if (persistent)
            {
                PersistentUIElements.Add(element);
            }
            else
            {
                UIElements.Add(element);
            }

            return element;
        }

        public void PopElement(IUIElement uiElement)
        {
            uiElement.RemoveInstance();
        }

        public void PopLayer(Action callback)
        {
            if (UIElements.Count == 0)
            {
                callback?.Invoke();
                return;
            }

            int elementCount = UIElements.Count;
            while (--elementCount >= 0)
            {
                UIElements[elementCount].ElementPoped -= ElementPoped;
                UIElements[elementCount].ElementPoped += (uiElement) =>
                {
                    PoppedLayerElement(callback);
                };
                UIElements[elementCount].RemoveInstance();
            }
        }

        private void ElementPoped(IUIElement popedElement)
        {
            if (UIElements.Contains(popedElement))
            {
                UIElements.Remove(popedElement);
            }
            else if (PersistentUIElements.Contains(popedElement))
            {
                PersistentUIElements.Remove(popedElement);
            }
        }

        private void PoppedLayerElement(Action callback)
        {
            popCount++;
            if (popCount == UIElements.Count)
            {
                UIElements.Clear();
                callback.Invoke();
            }
        }

        public void SetLayerGameObject(GameObject layerObject)
        {
            LayerGameObject = layerObject;
        }
    }
}