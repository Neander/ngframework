﻿using System;
using UnityEngine;

namespace NGFramework.Core.UI
{
    public interface IUIElement
    {
        /// <summary>
        /// The event that the Layer subscribes to when an instance is pushed into it.
        /// </summary>
        event Action<IUIElement> ElementPoped;

        /// <summary>
        /// Create a new instance of the UIElement and attach it to the supplied root.
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        IUIElement CreateInstance(Transform root);

        /// <summary>
        /// Start the removal sequence. This can be immediate or after an animation has played. Call
        /// ElementPoped when done.
        /// </summary>
        void RemoveInstance();
    }
}