﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace NGFramework.Core.UI
{
    public interface IUILayer
    {
        string LayerName { get; }

        GameObject LayerGameObject { get; }

        List<IUIElement> UIElements { get; }

        List<IUIElement> PersistentUIElements { get; }

        IUIElement PushElement(IUIElement uiElement, bool persistent = false);

        void PopElement(IUIElement uiElement);

        void PopLayer(Action callback);

        void SetLayerGameObject(GameObject layerObject);
    }
}