﻿using System;
using UnityEngine;

namespace NGFramework.Core.Sound
{
    public interface ISoundController
    {
        event Action<string, long, float, bool, bool, int> OnPlayAudio;

        event Action<string, long, AudioSource> OnPlayExternalSource;

        event Action<long, bool> OnStopAudio;

        event Action<long, bool> OnPauseAudio;

        event Action<long, float> OnChangeAudioVolume;

        event Action<long, bool> OnMuteAudio;

        bool MuteState { get; }

        SoundClipMeta PlayAudio(string soundClipId, AudioSource externalSource);

        SoundClipMeta PlayAudio(string soundClipId, float volume = 1, bool loop = false, int repeatAmount = 0);

        void StopAudio(long id, bool fade = false);

        void StopAll(bool fade = false);

        void PauseAll(bool paused);

        void PauseAudio(long id, bool paused);

        void SetMutedState(bool muted);

        void SetAudioVolume(float volume);
    }
}