﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace NGFramework.Core.Sound
{
    // TODO: Implement repeats
    public class SoundRepository : MonoBehaviour
    {
        private float fadeDuration = 1;
        private AudioLibrary audioLibrary;

        private List<long> fadingAudioSources;
        private List<AudioSource> audioSourcePool;
        private Dictionary<long, AudioSource> playingAudio;
        private List<long> externalSources;

        private ISoundController soundController;
        private ProjectContext projectContext;

        public void Intitialize(AudioLibrary audioLibrary)
        {
            this.audioLibrary = audioLibrary;

            soundController = ServiceLocator.Resolve<ISoundController>();
            projectContext = ServiceLocator.Resolve<ProjectContext>();

            fadingAudioSources = new List<long>();
            audioSourcePool = new List<AudioSource>();
            playingAudio = new Dictionary<long, AudioSource>();
            externalSources = new List<long>();

            soundController.OnPlayAudio += OnPlayAudio;
            soundController.OnPlayExternalSource += OnPlayExternalSource;
            soundController.OnStopAudio += OnStopAudio;
            soundController.OnPauseAudio += OnPauseAudio;
            soundController.OnMuteAudio += OnMuteAudio;
            soundController.OnChangeAudioVolume += OnChangeAudioVolume;
        }

        private void OnPlayExternalSource(string soundClipId, long id, AudioSource externalSource)
        {
            playingAudio.Add(id, externalSource);
            externalSources.Add(id);

            SoundClip soundClip = audioLibrary.clips.FirstOrDefault(c => c.id == soundClipId);

            externalSource.clip = soundClip.clip;
            externalSource.Play();
        }

        private void OnPlayAudio(string soundClipId, long id, float volume, bool muted, bool loop, int repeatAmount)
        {
            AudioSource currentSource = null;
            if (audioSourcePool.Count == 0)
            {
                GameObject newGO = new GameObject("AudioSource");
                currentSource = newGO.AddComponent<AudioSource>();
                currentSource.transform.SetParent(transform);
            }
            else
            {
                currentSource = audioSourcePool[0];
                currentSource.gameObject.SetActive(true);
                audioSourcePool.RemoveAt(0);
            }

            playingAudio.Add(id, currentSource);

            SoundClip soundClip = audioLibrary.clips.FirstOrDefault(c => c.id == soundClipId);

            currentSource.clip = soundClip.clip;
            currentSource.volume = volume;
            currentSource.loop = loop;
            currentSource.mute = muted;

            currentSource.Play();
        }

        private void OnStopAudio(long id, bool fade)
        {
            if (playingAudio.ContainsKey(id))
            {
                AudioSource audioSource = playingAudio[id];
                if (audioSource != null)
                {
                    if (fade)
                    {
                        if (!fadingAudioSources.Contains(id))
                        {
                            fadingAudioSources.Add(id);
                            StartCoroutine(FadeOut(id, audioSource));
                        }
                    }
                    else
                    {
                        StopAndPool(id, audioSource);
                    }
                }
            }
        }

        private void OnPauseAudio(long id, bool paused)
        {
            if (playingAudio.ContainsKey(id))
            {
                if (paused)
                {
                    playingAudio[id].Pause();
                }
                else
                {
                    playingAudio[id].UnPause();
                }
            }
        }

        private void OnMuteAudio(long id, bool mute)
        {
            if (playingAudio.ContainsKey(id))
            {
                playingAudio[id].mute = mute;
            }
        }

        private void OnChangeAudioVolume(long id, float newVolume)
        {
            AudioSource audioSource = playingAudio[id];
            if (audioSource != null)
            {
                audioSource.volume = newVolume;
            }
        }

        private void Update()
        {
            if (!projectContext.Paused)
            {
                int i = playingAudio.Count;
                while (--i >= 0)
                {
                    long key = playingAudio.Keys.ElementAt(i);
                    if (!playingAudio[key].isPlaying)
                    {
                        StopAndPool(key, playingAudio[key]);
                    }
                }
            }
        }

        private IEnumerator FadeOut(long id, AudioSource audioSource)
        {
            float t = fadeDuration;
            float step = 1 / t;
            while (t > 0)
            {
                audioSource.volume -= Time.deltaTime * step;
                t -= Time.deltaTime;
                yield return null;
            }

            StopAndPool(id, audioSource);
            fadingAudioSources.Remove(id);
        }

        private void StopAndPool(long id, AudioSource audioSource)
        {
            audioSource.Stop();
            playingAudio.Remove(id);

            if (!externalSources.Contains(id))
            {
                audioSource.gameObject.SetActive(false);
                audioSourcePool.Add(audioSource);
            }
            else
            {
                externalSources.Remove(id);
            }
        }

        private void OnDestroy()
        {
            soundController.OnPlayAudio -= OnPlayAudio;
            soundController.OnStopAudio -= OnStopAudio;
            soundController.OnPauseAudio -= OnPauseAudio;
            soundController.OnMuteAudio -= OnMuteAudio;
            soundController.OnChangeAudioVolume -= OnChangeAudioVolume;
        }
    }
}