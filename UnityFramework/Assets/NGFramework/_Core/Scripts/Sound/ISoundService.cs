﻿using System.Collections.Generic;

namespace NGFramework.Core.Sound
{
    public interface ISoundService
    {
        ICollection<ISound> GetAllPlayingAudio();

        ISound GetPlayingAudio(long id);

        void RemovePlayingAudio(long id);

        long AddAudio();
    }
}