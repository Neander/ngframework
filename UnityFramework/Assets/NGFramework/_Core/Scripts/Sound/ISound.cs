﻿namespace NGFramework.Core.Sound
{
    public interface ISound
    {
        long Id { get; set; }
    }
}