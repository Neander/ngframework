﻿using NGFramework.Core.Utils;
using System.Collections.Generic;
using System.Linq;

namespace NGFramework.Core.Sound
{
    public class SoundService : ISoundService
    {
        private ICollection<ISound> playingAudio;

        public SoundService()
        {
            playingAudio = new List<ISound>();
        }

        public ICollection<ISound> GetAllPlayingAudio()
        {
            return playingAudio;
        }

        public ISound GetPlayingAudio(long id)
        {
            return playingAudio.SingleOrDefault(x => x.Id == id);
        }

        public long AddAudio()
        {
            long id = IDGenerator.GenerateUniqueNumberID();
            Sound audio = new Sound
            {
                Id = id
            };
            playingAudio.Add(audio);
            return id;
        }

        public void RemovePlayingAudio(long id)
        {
            ISound sound = GetPlayingAudio(id);
            if (sound != null)
            {
                playingAudio.Remove(sound);
            }
        }
    }
}