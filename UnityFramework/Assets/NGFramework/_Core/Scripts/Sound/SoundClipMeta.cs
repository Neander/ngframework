﻿using System;

namespace NGFramework.Core.Sound
{
    [Serializable]
    public class SoundClipMeta
    {
        public long ID;
        public float Lenght;

        public SoundClipMeta(long id, float lenght)
        {
            ID = id;
            Lenght = lenght;
        }
    }
}