﻿using NGFramework.Core.Sound;
using NGFramework.Core.Utils;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace NGFramework.Core.Editor.Sound
{
    [CustomEditor(typeof(AudioLibrary), true)]
    [CanEditMultipleObjects]
    public class AudioLibraryEditor : UnityEditor.Editor
    {
        private string searchText;
        private string loweredText;
        private bool isDirty;

        public override void OnInspectorGUI()
        {
            AudioLibrary targetLibrary = target as AudioLibrary;

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Search: ", new GUILayoutOption[] { GUILayout.MaxWidth(50) });
            searchText = GUILayout.TextField(searchText);
            EditorGUILayout.EndHorizontal();

            GUILayout.Space(10);
            if (GUILayout.Button("Add AudioClip Slot"))
            {
                List<SoundClip> clips = targetLibrary.clips.ToList();
                string id = IDGenerator.GenerateUniqueID(8);
                clips.Add(new SoundClip(id));
                targetLibrary.clips = clips.ToArray();

                isDirty = true;
            }
            GUILayout.Space(20);

            List<SoundClip> currentClips = targetLibrary.clips.ToList();
            SoundClip[] soundClips = targetLibrary.clips;
            if (!string.IsNullOrEmpty(searchText))
            {
                loweredText = searchText.ToLower();
                soundClips = targetLibrary.clips.Where(x =>
                {
                    if (x.id.ToLower().Contains(loweredText) || x.alias.ToLower().Contains(loweredText))
                    {
                        return true;
                    }
                    else if (x.clip != null && x.clip.name.ToLower().Contains(loweredText))
                    {
                        return true;
                    }
                    return false;
                }).ToArray();
            }

            foreach (SoundClip clip in soundClips)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.SelectableLabel("ID: " + clip.id, new GUILayoutOption[] { GUILayout.Height(16) });
                EditorGUILayout.LabelField("Alias: ", new GUILayoutOption[] { GUILayout.MaxWidth(50) });
                clip.alias = EditorGUILayout.TextField(clip.alias);

                EditorGUILayout.EndHorizontal();

                EditorGUILayout.BeginHorizontal();

                clip.clip = (AudioClip)EditorGUILayout.ObjectField(clip.clip, typeof(AudioClip), false);

                if (GUILayout.Button("X", new GUILayoutOption[] { GUILayout.MaxWidth(30) }))
                {
                    currentClips.Remove(clip);
                    isDirty = true;
                }

                EditorGUILayout.EndHorizontal();

                GUILayout.Space(10);
            }
            targetLibrary.clips = currentClips.ToArray();

            EditorUtility.SetDirty(targetLibrary);

            if (isDirty)
            {
                isDirty = false;
                AssetDatabase.SaveAssets();
            }
        }
    }
}