using System;
using UnityEngine;

namespace NGFramework.Core.Sound
{
    [CreateAssetMenu(fileName = "AudioLibrary", menuName = "NGFramework/AudioLibrary")]
    public class AudioLibrary : ScriptableObject
    {
        public SoundClip[] clips = new SoundClip[0];
    }

    [Serializable]
    public class SoundClip
    {
        public string id;
        public string alias;
        public AudioClip clip;

        public SoundClip()
        {
        }

        public SoundClip(string id)
        {
            this.id = id;
        }
    }
}