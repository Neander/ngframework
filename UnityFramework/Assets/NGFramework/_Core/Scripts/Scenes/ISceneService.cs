﻿using System;

namespace NGFramework.Core.SceneManagement
{
    public interface ISceneService
    {
        Scenes ActiveScene { get; }

        void LoadScene(Scenes scene, Action callback);
    }
}