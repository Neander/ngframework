﻿using NGFramework.UI;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace NGFramework.Core.SceneManagement
{
    public class SceneService : ISceneService
    {
        public Scenes ActiveScene { get; private set; }

        private Action currentSceneLoadedAction;
        private AsyncOperation currentOperation;

        private bool canInstantiateScene;

        public void LoadScene(Scenes scene, Action callback)
        {
            currentSceneLoadedAction = callback;
            SceneManager.sceneLoaded += SceneLoaded;
            SceneManager.LoadScene(scene.ToString());
        }

        private void SceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
        {
            currentSceneLoadedAction?.Invoke();

            currentSceneLoadedAction = null;
            SceneManager.sceneLoaded -= SceneLoaded;
        }

        public void LoadSceneAsync(Scenes scene, Action callback)
        {
            UIController uiController = ServiceLocator.Resolve<UIController>();

            AsyncOperation operation = SceneManager.LoadSceneAsync(scene.ToString(), LoadSceneMode.Single);
            if (uiController != null && uiController.ReadyForTransition)
            {
                operation.allowSceneActivation = true;
                callback?.Invoke();
            }
            else
            {
                operation.allowSceneActivation = false;

                currentOperation = operation;
                currentSceneLoadedAction = callback;
            }
        }

        public void LoadSceneWhenDone()
        {
            if (currentOperation != null)
            {
                currentOperation.allowSceneActivation = true;
                currentSceneLoadedAction?.Invoke();

                currentOperation = null;
                currentSceneLoadedAction = null;
            }
        }

        private void Completed(AsyncOperation asyncOperation)
        {
            asyncOperation.allowSceneActivation = true;
            Debug.Log("Completed");
        }
    }
}