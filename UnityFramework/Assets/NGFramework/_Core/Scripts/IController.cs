﻿using System;

namespace NGFramework.Core
{
    public interface IController
    {
        /// <summary>
        /// Call this to initialize the controller. The callback should be called when done
        /// initializing with the name of the controller.
        /// </summary>
        /// <param name="onIntializedCallback"></param>
        void Initialize(Action<string> onIntializedCallback);
    }
}